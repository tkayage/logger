Methods in Logger module are:
    log():
        Parameters: 
            type: Error or Info
            service_name: Service being accessed 
            source: Source of the error
            data: metadata for the error
        Return: null
            