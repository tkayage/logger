let fs = require('fs');

let config = require('./config.json');

const logsDirectory = config.logsDirectory;
const maxSize = 10000000;


class Logger {
    constructor() {
        if (!fs.existsSync(logsDirectory + 'errors/error.log')) {
            fs.open(logsDirectory + 'errors/error.log', 'w', function (err, errFile) {
                if (err) console.log(err);
            });
        }
        if (!fs.existsSync(logsDirectory + 'info/info.log')) {
            fs.open(logsDirectory + 'info/info.log', 'w', function (err, infoFile) {
                if (err) console.log(err);
            });
        }
    }

    log(type, service_name, source, log) {

        let preparedLog = prepareLog(type, service_name, source, log);
        _log(type, preparedLog);

    }
}

function archive(type, callback) {
    let newFilename;
    switch (type) {
        case 'error':
            newFilename = getDateTime() + "error.log";
            fs.rename(logsDirectory + "errors/error.log", logsDirectory + newFilename, function (err) {
                if (!err) {
                    new Logger().log("info", "Logger", "SYS", newFilename + " file archived");
                    callback({ code: 200, data: newFilename + " file archived" });
                } else {
                    new Logger().log("error", "Logger", "SYS", err);
                    callback({ code: 1022, data: err });
                }
            });
            break;

        case 'info':
            newFilename = getDateTime() + "info.log";
            fs.rename(logsDirectory + "info/info.log", logsDirectory + "info/" + newFilename, function (err) {
                if (!err) {
                    new Logger().log("info", "Logger", "SYS", newFilename + " file archived");
                    callback({ code: 200, data: newFilename + " file archived" });
                } else {
                    new Logger().log("error", "Logger", "SYS", err);
                    callback({ code: 1022, data: err });
                }
            });
            break;
        default:
            break;
    }
}

function prepareLog(type, service_name, source, log) {
    let preparedLog = "";
    let code = "null";

    switch (type) {
        case 'error':
            preparedLog = "\n" + "[ERR] " + getDateTime() + "\t | " + service_name + "\t | " + source + ": " + process.pid + "\t | " + code + "\t | " + log;
            break;

        case 'info':
            preparedLog = "\n" + "[INF] " + getDateTime() + "\t | " + service_name + "\t | " + source + ": " + process.pid + "\t | " + code + "\t | " + log;
            break;
        default:
            break;
    }
    return preparedLog;
}

function isFull(file) {
    if (fs.statSync(file)["size"] < maxSize) return false;
    else return true;
}

function _log(type, preparedLog) {
    if (type === "error") {
        if (isFull(logsDirectory + "errors/error.log")) {
            archive(type, (a) => {
                if (a.code === 200) {
                    fs.appendFile(logsDirectory + "errors/error.log", preparedLog, function (err) {
                        if (err) console.log("error", "Logger", "SYS", err);
                        console.log(preparedLog);
                    });
                } else {
                    console.log(a);
                }
            });

        } else {
            fs.open(logsDirectory + "errors/error.log", "a+", (err, fd) => {
                if (err) console.log(err);
                fs.appendFile(fd, preparedLog, function (err) {
                    if (err) console.log("error", "Logger", "SYS", err);
                    console.log(preparedLog);
                });
            });
        }
    } else {
        if (isFull(logsDirectory + "info/info.log")) {
            archive(type, (a) => {
                if (a.code === 200) {
                    fs.appendFile(logsDirectory + "info/info.log", preparedLog, function (err) {
                        if (err) console.log(err);
                        console.log(preparedLog);
                    });
                } else {
                    console.log(a);
                }
            });
        } else {
            fs.open(logsDirectory + "info/info.log", "a+", (err, fd) => {
                if (err) console.log(err);
                fs.appendFile(fd, preparedLog, function (err) {
                    if (err) console.log("error", "Logger", "SYS", err);
                    console.log(preparedLog);
                });
            });
        }
    }
}

function getDateTime(date = new Date()) {
    let mon = date.getMonth() + 1;
    return date.getFullYear() + "-" + mon + "-" + date.getDate() + "  " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}

module.exports = new Logger();
